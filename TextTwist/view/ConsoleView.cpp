#include "ConsoleView.h"

namespace view {

    ConsoleView::ConsoleView()
    {
        //ctor
    }

    // currently prints all words from file that are 3 letters or more. The test file used only has 10 words.
    void ConsoleView::printWordBank(WordBank words) {
        cout << "Current Words Available" << endl;
        unordered_map<string, int> wordMap = words.getAvailableWords();
        for(const auto& word : wordMap ) {
            cout << "Word: " << word.first << endl;
            cout << "Point Value: " << word.second<< endl;
        }

        cout << "" << endl;
    }

    void ConsoleView::testPrintLetterRackMap(unordered_map<char, int> letters) {
        for(const auto& letter : letters ) {
            cout << "Word: " << letter.first << ", Point Value: " << letter.second << "\n";
        }

        cout << "" << endl;
    }

    // Uncomment loop below to see all letters in bag, prints number of letters in vector. Should always be 97 based on given freqs.
    void ConsoleView::printLetterBag(LetterBag letters) {
        cout << "Entire Letter Set" << endl;
        vector<char> letterSet = letters.getLetterBag();
        //for (int i = 0; i < letterSet.size(); i++) {
            //cout << letterSet[i] << endl;
        //}
        cout << "Total letter count: " << letterSet.size() << endl;
        cout << "" << endl;

    }

    // Prints current letters for player use
    void ConsoleView::printLetterRack(LetterRack playerLetters) {
        cout << "Player's letters to use" << endl;
        vector<char> letterRack = playerLetters.getLetterRack();
         for (int i = 0; i < letterRack.size(); i++) {
            cout << letterRack[i] << " " << "";
        }
        cout << "" << endl;
    }

    ConsoleView::~ConsoleView()
    {
        //dtor
    }
}
