#include "GameWindowView.h"
#include "TextTwistController.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace controller;
namespace view
{
GameWindowView::GameWindowView(int width, int height, const char* title) : Fl_Window(width, height, title)
{
    begin();
    this->textTwistController = new TextTwistController();
    this->enteredWordsBuffer = new Fl_Text_Buffer();
    this->userInputBuffer = new Fl_Text_Buffer();
    this->userRackBuffer = new Fl_Text_Buffer();
    this->scoreOutputBuffer = new Fl_Text_Buffer();
    this->timerBuffer = new Fl_Text_Buffer();
    this->highScoreBuffer = new Fl_Text_Buffer();
    this->wordsRemainingBuffer = new Fl_Text_Buffer();

    this->timerDisplay = new Fl_Text_Display(width-110, 270, 100, 25);
    this->userInputField = new Fl_Text_Editor(178, 150, 150, 25);
    this->userRackDisplay = new Fl_Text_Display(75, 20, 325, 25);
    this->scoreOutputField = new Fl_Text_Display(10, 270, 100, 25);
    this->enteredWordsDisplay = new Fl_Text_Display(10, 305, 230 ,190);
    this->highScoreDisplay = new Fl_Text_Display(250, 305, 240 ,190);
    this->wordsRemainingDisplay = new Fl_Text_Display(width-140, 235, 130, 25);

    this->highScoreDisplay->buffer(highScoreBuffer);
    this->enteredWordsDisplay->buffer(enteredWordsBuffer);
    this->userInputField->textfont(FL_COURIER);
    this->userInputField->buffer(userInputBuffer);
    this->timerDisplay->buffer(timerBuffer);
    this->userRackDisplay->textfont(FL_COURIER);
    this->userRackDisplay->buffer(userRackBuffer);
    this->scoreOutputField->buffer(scoreOutputBuffer);
    this->wordsRemainingDisplay->buffer(wordsRemainingBuffer);

    this->submitButton = new Fl_Button(205, 200, 100, 30, "Submit Word");
    this->submitButton->callback(cbSubmit, this);

    this->twistButton = new Fl_Button(415, 20, 70, 30, "Twist");
    this->twistButton->callback(cbTwist, this);

    this->oneMinuteButton = new Fl_Button(10, 120, 80, 30, "1 Minute");
    this->oneMinuteButton->callback(cbOneMinute, this);

    this->twoMinuteButton = new Fl_Button(10, 170, 80, 30, "2 Minutes");
    this->twoMinuteButton->callback(cbTwoMinute, this);

    this->threeMinuteButton = new Fl_Button(10, 220, 80, 30, "3 Minutes");
    this->threeMinuteButton->callback(cbThreeMinute, this);

    this->allowLetterReuse = new Fl_Button(10, 70, 160, 30, "Toggle Reuse Letters");
    this->allowLetterReuse->callback(cbLetterReuse, this);

    this->restartButton = new Fl_Button(205, 250, 100, 30, "Restart");
    this->restartButton->callback(cbRestart, this);
    end();
}

void GameWindowView::start()
{
    textTwistController->run();
    this->timerCount = this->textTwistController->timer;
    this->timerBuffer->text("Time: ");
    this->enteredWordsBuffer->text("Word: \t PointValue: ");
    this->highScoreBuffer->text("Score: \t Time(secs): ");
    this->setHighScoreOutput(this->textTwistController->allScores, this->textTwistController->allTimes);
    this->timerBuffer->append(to_string(this->timerCount).c_str());
    this->scoreOutputBuffer->text("Score: 0");
    this->assignButtons();
    this->wordsRemainingBuffer->text("Words Left: ");
    this->wordsRemainingBuffer->append(to_string(this->textTwistController->wordBank.getWordCount()).c_str());
    this->show();
}

void GameWindowView::assignButtons()
{
    char l1 = this->textTwistController->playerLetters.getLetterRack()[0];
    char l2 = this->textTwistController->playerLetters.getLetterRack()[1];
    char l3 = this->textTwistController->playerLetters.getLetterRack()[2];
    char l4 = this->textTwistController->playerLetters.getLetterRack()[3];
    char l5 = this->textTwistController->playerLetters.getLetterRack()[4];
    char l6 = this->textTwistController->playerLetters.getLetterRack()[5];
    char l7 = this->textTwistController->playerLetters.getLetterRack()[6];

    string whitespace = "    ";
    string let1(1, l1);
    string let2(1, l2);
    string let3(1, l3);
    string let4(1, l4);
    string let5(1, l5);
    string let6(1, l6);
    string let7(1, l7);

    this->userRackBuffer->append(whitespace.c_str());
    this->userRackBuffer->append(let1.c_str());
    this->userRackBuffer->append(whitespace.c_str());
    this->userRackBuffer->append(let2.c_str());
    this->userRackBuffer->append(whitespace.c_str());
    this->userRackBuffer->append(let3.c_str());
    this->userRackBuffer->append(whitespace.c_str());
    this->userRackBuffer->append(let4.c_str());
    this->userRackBuffer->append(whitespace.c_str());
    this->userRackBuffer->append(let5.c_str());
    this->userRackBuffer->append(whitespace.c_str());
    this->userRackBuffer->append(let6.c_str());
    this->userRackBuffer->append(whitespace.c_str());
    this->userRackBuffer->append(let7.c_str());
    this->userRackBuffer->append(whitespace.c_str());
}

void GameWindowView::Timer_CB(void *data)
{
    GameWindowView* window = (GameWindowView*)data;
    if ( window->timerCount < 1 )
    {
        Fl::remove_timeout(Timer_CB, data);
        window->writeHighScore();
    }
    else
    {
        window->timerCount = window->timerCount-1;
        window->timerBuffer->text("Time: ");
        window->timerBuffer->append(to_string(window->timerCount).c_str());
        Fl::repeat_timeout(1, Timer_CB, data);
    }
}

void GameWindowView::cbRestart(Fl_Widget* widget, void *data)
{
    GameWindowView* window = (GameWindowView*)data;
    window->textTwistController = new TextTwistController();
    window->userInputBuffer->text("");
    window->userRackBuffer->text("");
    window->scoreOutputBuffer->text("Score: 0");
    window->start();
}

void GameWindowView::cbOneMinute(Fl_Widget* widget, void *data)
{
    GameWindowView* window = (GameWindowView*)data;
    window->textTwistController->timer = 60;
    window->writeRefreshedSettings();
    window->textTwistController = new TextTwistController();
    window->userInputBuffer->text("");
    window->userRackBuffer->text("");
    window->scoreOutputBuffer->text("Score: 0");
    window->start();
}

void GameWindowView::cbTwoMinute(Fl_Widget* widget, void *data)
{
    GameWindowView* window = (GameWindowView*)data;
    window->textTwistController->timer = 120;
    window->writeRefreshedSettings();
    window->textTwistController = new TextTwistController();
    window->userInputBuffer->text("");
    window->userRackBuffer->text("");
    window->scoreOutputBuffer->text("Score: 0");
    window->start();
}

void GameWindowView::cbThreeMinute(Fl_Widget* widget, void *data)
{
    GameWindowView* window = (GameWindowView*)data;
    window->textTwistController->timer = 180;
    window->writeRefreshedSettings();
    window->textTwistController = new TextTwistController();
    window->userInputBuffer->text("");
    window->userRackBuffer->text("");
    window->scoreOutputBuffer->text("Score: 0");
    window->start();
}

void GameWindowView::cbLetterReuse(Fl_Widget* widget, void *data)
{
    GameWindowView* window = (GameWindowView*)data;
    window->textTwistController->reuseLetters = !window->textTwistController->reuseLetters;
    window->writeRefreshedSettings();
    window->textTwistController = new TextTwistController();
    window->userInputBuffer->text("");
    window->userRackBuffer->text("");
    window->scoreOutputBuffer->text("Score: 0");
    window->start();
}

void GameWindowView::writeRefreshedSettings()
{
    std::ofstream ofs;
    ofs.open ("settings.txt", ios::out | ios::trunc);

    ofs << this->textTwistController->timer;
    ofs << ",";
    ofs << this->textTwistController->reuseLetters;

    ofs.close();
}

void GameWindowView::cbSubmit(Fl_Widget* widget, void *data)
{
    GameWindowView* window = (GameWindowView*)data;

    string input = window->userInputBuffer->text();
    cout << input << endl;
    window->userInputBuffer->text("");
    //this->gameWindow->show();

    bool validWord = false;
    bool usedWord = false;

    for (int i = 0; i < window->correctWords.size(); i++)
    {
        if(input == window->correctWords[i])
        {
            usedWord = true;
        }
    }

    for(const auto& word : window->textTwistController->wordBank.getAvailableWords() )
    {
        string firstWord = word.first;

        if(input == firstWord)
        {
            window->textTwistController->currentScore += word.second;
            window->setWordsEnteredOutputText(firstWord, word.second);
            window->correctWords.push_back(input);
            window->textTwistController->wordBank.removeUsedWord(input);
            validWord = true;
            window->wordsRemainingBuffer->text("Words Left: ");
            window->wordsRemainingBuffer->append(to_string(window->textTwistController->wordBank.getAvailableWords().size()).c_str());
        }

    }

    if(!validWord && !usedWord)
    {
        cout << "The entered word was invalid" << endl;
        window->textTwistController->currentScore -= 10;
    }
    string score = std::to_string(window->textTwistController->currentScore);
    window->scoreOutputBuffer->text("Score: ");
    window->setScoreOutputText(score);
    cout << "Current Score: " + std::to_string(window->textTwistController->currentScore) << endl;
}

void GameWindowView::setScoreOutputText(const string& outputText)
{
    this->scoreOutputBuffer->append(outputText.c_str());
}

void GameWindowView::setWordsEnteredOutputText(const string& word, int pointValue) {
    this->enteredWordsBuffer->append("\n");
    this->enteredWordsBuffer->append(word.c_str());
    this->enteredWordsBuffer->append("\t");
    this->enteredWordsBuffer->append(to_string(pointValue).c_str());
}

void GameWindowView::setHighScoreOutput(vector<int> scores, vector<int> times) {
    int score;
    int time;

    for (int i = 0; i < scores.size(); i++) {
        score = scores[i];
        time = times[i];
        this->highScoreBuffer->append("\n");
        this->highScoreBuffer->append(to_string(score).c_str());
        this->highScoreBuffer->append("\t");
        this->highScoreBuffer->append(to_string(time).c_str());
    }
}

void GameWindowView::writeHighScore()
{
    fstream myfile;
    myfile.open ("scores.txt", fstream::in | fstream::out | fstream::app);
    myfile << to_string(this->textTwistController->currentScore).c_str();
    myfile << (",");
    myfile << to_string(this->textTwistController->timer).c_str();
    myfile.close();
}

void GameWindowView::cbTwist(Fl_Widget* widget, void *data)
{
    GameWindowView* window = (GameWindowView*)data;
    window->textTwistController->playerLetters.twistLetterRack();
    window->userRackBuffer->text("");
    window->assignButtons();
}

string GameWindowView::getChosenWord()
{
    return this->getChosenWord();
}


GameWindowView::~GameWindowView()
{
    //dtor
}
}

