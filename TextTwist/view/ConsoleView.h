#ifndef CONSOLEVIEW_H
#define CONSOLEVIEW_H

#include "WordBank.h"
#include "LetterBag.h"
#include "LetterRack.h"
using namespace model;

#include <unordered_map>
#include <iostream>
#include <string>
using namespace std;

namespace view {
    class ConsoleView
    {
        public:
            ConsoleView();
            virtual ~ConsoleView();

            void printWordBank(WordBank words);
            void printLetterBag(LetterBag letters);
            void printLetterRack(LetterRack playerLetters);
            void testPrintLetterRackMap(unordered_map<char, int> letters);
        protected:
        private:
    };
}

#endif // CONSOLEVIEW_H
