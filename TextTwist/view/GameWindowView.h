#ifndef GAMEWINDOWVIEW_H
#define GAMEWINDOWVIEW_H
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Text_Editor.H>
#include <FL/Fl_Text_Buffer.H>
#include <FL/Fl_Button.H>
#include <string>
#include <TextTwistController.h>
#include <vector>
using namespace controller;
using namespace std;

namespace view {
class GameWindowView : public Fl_Window
{
    public:
        GameWindowView(int width, int height, const char* title);
        static void Timer_CB(void *data);
        virtual ~GameWindowView();
        string getChosenWord();
        void start();

        void setScoreOutputText(const string& outputText);
        void setWordsEnteredOutputText(const string& word, int pointValue);

    protected:
    private:
        int timerCount;

        Fl_Text_Editor *userInputField;
        Fl_Text_Buffer *userInputBuffer;

        Fl_Text_Display *enteredWordsDisplay;
        Fl_Text_Buffer *enteredWordsBuffer;

        Fl_Text_Display *highScoreDisplay;
        Fl_Text_Buffer *highScoreBuffer;

        Fl_Text_Buffer *scoreOutputBuffer;
        Fl_Text_Display *scoreOutputField;

        Fl_Text_Buffer *userRackBuffer;
        Fl_Text_Display *userRackDisplay;

        Fl_Text_Display *timerDisplay;
        Fl_Text_Buffer *timerBuffer;

        Fl_Text_Display *wordsRemainingDisplay;
        Fl_Text_Buffer *wordsRemainingBuffer;

        Fl_Button *twistButton;
        Fl_Button *submitButton;
        Fl_Button *restartButton;
        Fl_Button *oneMinuteButton;
        Fl_Button *twoMinuteButton;
        Fl_Button *threeMinuteButton;
        Fl_Button *allowLetterReuse;

        static void cbLetterReuse(Fl_Widget* widget, void *data);
        static void cbSubmit(Fl_Widget* widget, void *data);
        static void cbTwist(Fl_Widget* widget, void *data);
        static void cbRestart(Fl_Widget* widget, void *data);
        static void cbOneMinute(Fl_Widget* widget, void *data);
        static void cbTwoMinute(Fl_Widget* widget, void *data);
        static void cbThreeMinute(Fl_Widget* widget, void *data);
        void assignButtons();
        void writeHighScore();
        void writeRefreshedSettings();
        void setHighScoreOutput(vector<int> scores, vector<int> times);

        Fl_Button *letter1;
        Fl_Button *letter2;
        Fl_Button *letter3;
        Fl_Button *letter4;
        Fl_Button *letter5;
        Fl_Button *letter6;
        Fl_Button *letter7;

        TextTwistController *textTwistController;


        string chosenWord;
        vector<string> correctWords;
    };
}

#endif // GAMEWINDOWVIEW_H
