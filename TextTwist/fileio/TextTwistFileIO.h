#ifndef TEXTTWISTFILEIO_H
#define TEXTTWISTFILEIO_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <string>
#include <array>
using namespace std;

#include "WordBank.h"
#include "LetterRack.h"
using namespace model;

namespace fileio {

    class TextTwistFileIO
    {
        public:
            TextTwistFileIO();
            virtual ~TextTwistFileIO();

            void loadSettingsFromFile(const string& fileName);
            void loadHighScoresFromFile(const string& fileName);
            WordBank loadWordBankFromFile(WordBank wordBank, const string& fileName, LetterRack playerLetters, bool reuseLetters);
            unordered_map<char, int> getPlayerLettersMap(vector<char> letters);

            bool getStatusOfLetterRack();
            int getTimerValue();

            vector<int> getTimeOfScores();
            vector<int> getPointsScored();

        protected:
        private:
            int setPointValueOfWord(const string& word);
            int minWordLength;
            int maxWordLength;
            bool isWordLongEnough(const string& word, bool rackStatus);
            bool determineIfWordCanBeMadeFromLetters(const string& word, vector<char> letters, WordBank wordBank);
            bool determineIfWordCanBeMadeFromLettersWhenReusing(const string& word, vector<char> letters, WordBank wordBank);
            unordered_map<char, int> letterCountOfGivenWord(const string& word);
            unordered_map<char, int> letterCountOfLetterRack(vector<char> letters);
            unordered_map<char, int> initializedAlphabetMap;
            vector<char> alphabetVector;

            bool unlockedLetterRack;
            int timer;

            vector<int> timeOfScores;
            vector<int> pointsScored;
    };
}

#endif // TEXTTWISTFILEIO_H
