#include "LetterFrequencyLoader.h"

namespace fileio {

    LetterFrequencyLoader::LetterFrequencyLoader()
    {
        this->letters = unordered_map<char, int>();
    }

    unordered_map<char, int> LetterFrequencyLoader::loadLetterFreqFromFile(const string& fileName) {
        string line, letter, frequency;
        ifstream myFile(fileName);
        char theLetter;
        int theFreq;

        if (myFile.is_open())
        {
            while (getline(myFile,line))
            {
                istringstream fileLine(line);
                getline(fileLine, letter, ',');
                getline(fileLine, frequency);
                theLetter = letter[0];
                theFreq = std::stoi(frequency);
                this->letters[theLetter] = theFreq;
            }
            myFile.close();
        }
        return this->letters;
    }

    LetterFrequencyLoader::~LetterFrequencyLoader()
    {
        //dtor
    }
}
