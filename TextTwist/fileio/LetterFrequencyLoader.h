#ifndef LETTERFREQUENCYLOADER_H
#define LETTERFREQUENCYLOADER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <string>
#include <array>
using namespace std;

#include "WordBank.h"
#include "LetterRack.h"
#include "LetterBag.h"
using namespace model;

namespace fileio {

    class LetterFrequencyLoader
    {
        public:
            LetterFrequencyLoader();
            virtual ~LetterFrequencyLoader();

            unordered_map<char, int> loadLetterFreqFromFile(const string& fileName);
        protected:
        private:
            unordered_map<char, int> letters;
    };
}

#endif // LETTERFREQUENCYLOADER_H
