#include "TextTwistFileIO.h"

namespace fileio {

    TextTwistFileIO::TextTwistFileIO()
    {
        this->minWordLength = 3;
        this->maxWordLength = 7;
        this->unlockedLetterRack = false;
        this->timer = 60;

        this->initializedAlphabetMap = unordered_map<char, int>() = {
            {'e', 0}, {'t', 0}, {'o', 0}, {'a', 0}, {'i', 0},
            {'n', 0}, {'s', 0}, {'h', 0}, {'r', 0}, {'l', 0},
            {'d', 0}, {'u', 0}, {'w', 0}, {'y', 0}, {'b', 0},
            {'c', 0}, {'f', 0}, {'g', 0}, {'m', 0}, {'p', 0},
            {'v', 0}, {'j', 0}, {'k', 0}, {'q', 0}, {'x', 0}, {'z', 0}
        };

        this->alphabetVector = vector<char>() = {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z'};
    }

    void TextTwistFileIO::loadSettingsFromFile(const string& fileName) {
        string line, time, rackSetting;
        int booleanValue;
        ifstream myFile(fileName);
        if(myFile.is_open()) {
            while (getline(myFile, line)) {
                istringstream settingsLine(line);
                getline(settingsLine, time, ',');
                getline(settingsLine, rackSetting);
                this->timer = std::stoi(time);
                booleanValue = std::stoi(rackSetting);
                if (booleanValue == 1) {
                    this->unlockedLetterRack = true;
                }
            }
            myFile.close();
        }
    }

    void TextTwistFileIO::loadHighScoresFromFile(const string& fileName) {
        string line, points, time;
        ifstream myFile(fileName);
        if(myFile.is_open()) {
            while (getline(myFile, line)) {
                istringstream settingsLine(line);
                getline(settingsLine, points, ',');
                getline(settingsLine, time);
                this->pointsScored.push_back(std::stoi(points));
                this->timeOfScores.push_back(std::stoi(time));
            }
            myFile.close();
        }
    }

    int TextTwistFileIO::getTimerValue() {
        return this->timer;
    }

    bool TextTwistFileIO::getStatusOfLetterRack() {
        return this->unlockedLetterRack;
    }

    vector<int> TextTwistFileIO::getPointsScored() {
        return this->pointsScored;
    }

    vector<int> TextTwistFileIO::getTimeOfScores() {
        return this->timeOfScores;
    }

    WordBank TextTwistFileIO::loadWordBankFromFile(WordBank wordBank, const string& fileName, LetterRack playerLetters, bool reuseLetters) {
        string line, word;
        int pointValue;
        ifstream myFile(fileName);
        vector<char> letters = playerLetters.getLetterRack();

        if (myFile.is_open())
        {
            while (getline(myFile,line))
            {
                istringstream wordLine(line);
                getline(wordLine, word);
                word.erase(word.length()-1);
                if(reuseLetters == false) {
                    if (this->isWordLongEnough(word, reuseLetters) && this->determineIfWordCanBeMadeFromLetters(word, letters, wordBank)) {
                        pointValue = this->setPointValueOfWord(word);
                        wordBank.addToWordBank(word, pointValue);
                    }
                } else {
                    if (this->isWordLongEnough(word, reuseLetters) && this->determineIfWordCanBeMadeFromLettersWhenReusing(word, letters, wordBank)) {
                        pointValue = this->setPointValueOfWord(word);
                        wordBank.addToWordBank(word, pointValue);
                    }
                }
            }
            myFile.close();
        }

        return wordBank;
    }

    int TextTwistFileIO::setPointValueOfWord(const string& word) {
        return word.length() * 10 * word.length();
    }

    bool TextTwistFileIO::determineIfWordCanBeMadeFromLetters(const string& word, vector<char> letters, WordBank wordBank) {
        unordered_map<char, int> wordLetterCount(this->letterCountOfGivenWord(word));
        unordered_map<char, int> playerLetterCount(this->letterCountOfLetterRack(letters));
        bool wordCanBeMade = true;

        for (int i = 0; i < this->alphabetVector.size(); i++) {
            if(wordLetterCount[alphabetVector[i]] > playerLetterCount[alphabetVector[i]]) {
                wordCanBeMade = false;
            }
        }

        return wordCanBeMade;

    }


    bool TextTwistFileIO::determineIfWordCanBeMadeFromLettersWhenReusing(const string& word, vector<char> letters, WordBank wordBank) {
        unordered_map<char, int> wordLetterCount(this->letterCountOfGivenWord(word));
        unordered_map<char, int> playerLetterCount(this->letterCountOfLetterRack(letters));
        bool wordCanBeMade = true;

        for (int i = 0; i < this->alphabetVector.size(); i++) {
            if(wordLetterCount[alphabetVector[i]] > 0 && playerLetterCount[alphabetVector[i]] == 0) {
                wordCanBeMade = false;
            }
        }

        return wordCanBeMade;

    }

    unordered_map<char, int> TextTwistFileIO::getPlayerLettersMap(vector<char> letters) {
        unordered_map<char, int> theLetters(this->letterCountOfLetterRack(letters));
        return theLetters;
    }

    unordered_map<char, int> TextTwistFileIO::letterCountOfGivenWord(const string& word) {
        unordered_map<char, int> letterCount(this->initializedAlphabetMap);
        for(int i = 0; i < word.size(); i++) {
            int counter = letterCount[word[i]];
            counter++;
            letterCount.erase(word[i]);
            letterCount[word[i]] = counter;
        }
        return letterCount;
    }

    unordered_map<char, int> TextTwistFileIO::letterCountOfLetterRack(vector<char> letters) {
        unordered_map<char, int> letterCount(this->initializedAlphabetMap);
        for(int i = 0; i < letters.size(); i++) {
            int counter = letterCount[letters[i]];
            counter++;
            letterCount.erase(letters[i]);
            letterCount[letters[i]] = counter;
        }
        return letterCount;
    }

    bool TextTwistFileIO::isWordLongEnough(const string& word, bool rackStatus) {
    if(rackStatus) {
        return (word.length() >= this->minWordLength);
    } else {
        return (word.length() >= this->minWordLength && word.length() <= this->maxWordLength);
    }
    }

    TextTwistFileIO::~TextTwistFileIO()
    {
        //dtor
    }
}
