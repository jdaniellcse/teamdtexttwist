#include "TextTwistController.h"

namespace controller
{

TextTwistController::TextTwistController()
{
    this->currentScore = 0;
    this->dictFileName = "dictionary.txt";
    this->letterFreqFileName = "letters.txt";
    this->letters = LetterBag();
    this->playerLetters = LetterRack();
    this->wordBank = WordBank();
    this->fileLoader = TextTwistFileIO();
    this->letterLoader = LetterFrequencyLoader();
    this->consolePrinting = ConsoleView();
    //this->gameWindow = new GameWindowView(500,300,"test");

}

void TextTwistController::run()
{
    bool running = true;
    bool validWord = false;


    this->fileLoader.loadSettingsFromFile("settings.txt");
    this->fileLoader.loadHighScoresFromFile("scores.txt");
    this->timer = fileLoader.getTimerValue();
    this->reuseLetters = fileLoader.getStatusOfLetterRack();
    this->allScores = fileLoader.getPointsScored();
    this->allTimes = fileLoader.getTimeOfScores();
    unordered_map<char, int> letterFreq(this->letterLoader.loadLetterFreqFromFile(letterFreqFileName));
    this->letters.setLetterFreqMap(letterFreq);
    //this->letters.addLetterToFreqMap('a', 9);
    this->letters.populateLetterBag();

    this->playerLetters.initializeLetterRack(this->letters.getLetterBag());
    this->wordBank = this->fileLoader.loadWordBankFromFile(this->wordBank, dictFileName, this->playerLetters, this->reuseLetters);

    this->consolePrinting.printWordBank(this->wordBank);
    this->consolePrinting.printLetterBag(this->letters);
    this->consolePrinting.printLetterRack(this->playerLetters);
    this->playerLetters.twistLetterRack();
    this->consolePrinting.printLetterRack(this->playerLetters);

    cout << this->timer << endl;
    cout << this->fileLoader.getStatusOfLetterRack() << endl;

    this->wordBank.setWordCount();

    //this->gameWindow->show();

    //this->consolePrinting.testPrintLetterRackMap(this->fileLoader.getPlayerLettersMap(this->playerLetters.getLetterRack()));

    // Just called to show that the twistLetterRack randomizes and returns the same vector of letters.
}

TextTwistController::~TextTwistController()
{
    //dtor
}
}
