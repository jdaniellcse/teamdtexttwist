#ifndef TEXTTWISTCONTROLLER_H
#define TEXTTWISTCONTROLLER_H

#include <algorithm>

#include "WordBank.h"
#include "LetterBag.h"
#include "LetterRack.h"
using namespace model;

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

#include "TextTwistFileIO.h"
#include "LetterFrequencyLoader.h"
using namespace fileio;

#include "ConsoleView.h"
using namespace view;

using namespace std;

namespace controller
{

class TextTwistController
{
public:
    bool reuseLetters;
    LetterRack playerLetters;
    WordBank wordBank;
    int currentScore;
    int timer;
    vector<int> allScores;
    vector<int> allTimes;
    TextTwistController();
    virtual ~TextTwistController();

    void run();


protected:
private:

    string dictFileName;
    string letterFreqFileName;
    LetterBag letters;
    TextTwistFileIO fileLoader;
    LetterFrequencyLoader letterLoader;
    ConsoleView consolePrinting;

};
}

#endif // TEXTTWISTCONTROLLER_H
