#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>

#include "TextTwistController.h"
#include "GameWindowView.h"
using namespace controller;
using namespace view;

int main (int argc, char ** argv)
{
  GameWindowView* gameView = new GameWindowView(500,500,"Team D Text Twist");
  gameView->start();
  Fl::add_timeout(1, GameWindowView::Timer_CB, gameView);

  int exitCode = Fl::run();
  //console testing

}
