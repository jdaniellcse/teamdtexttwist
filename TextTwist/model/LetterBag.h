#ifndef LETTERBAG_H
#define LETTERBAG_H

#include <vector>
#include <unordered_map>
#include <ctime>
#include <algorithm>
#include <cstdlib>
using namespace std;

namespace model {

    class LetterBag
    {
        public:
            LetterBag();
            virtual ~LetterBag();

            void populateLetterBag();
            vector<char> getLetterBag();
            void setLetterBag(vector<char> letters);
            void setLetterFreqMap(unordered_map<char, int> letterFreqMap);
            void addLetterToFreqMap(char letter, int freq);
        protected:
        private:
            int defaultRandomizeNumber;
            vector<char> letters;
            unordered_map<char, int> letterFreqMap;
            void randomizeLetterBag();
            vector<char> randomizeLetterBagGivenNumberOfTimes(vector<char> letterVector, int number);
    };
}

#endif // LETTERBAG_H
