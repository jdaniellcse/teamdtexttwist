#include "LetterBag.h"

namespace model {

    LetterBag::LetterBag()
    {
        this->letterFreqMap = unordered_map<char, int>();

        this->letters = vector<char>();
        this->defaultRandomizeNumber = 10;
    }

    void LetterBag::populateLetterBag() {
        for(const auto& letter : this->letterFreqMap) {
            for(int i = 0; i <= letter.second - 1; i++) {
                this->letters.push_back(letter.first);
            }
        }
        this->randomizeLetterBag();
    }

    void LetterBag::randomizeLetterBag() {
        std::srand (unsigned (std::time(0)));
        vector<char> letterVector = this->getLetterBag();
        letterVector = this->randomizeLetterBagGivenNumberOfTimes(letterVector, this->defaultRandomizeNumber);
        this->setLetterBag(letterVector);
    }

    vector<char> LetterBag::randomizeLetterBagGivenNumberOfTimes(vector<char> letterVector, int number) {
        for(int i = 0; i < number; i++) {
            std::random_shuffle (letterVector.begin(), letterVector.end());
        }
        return letterVector;
    }

    vector<char> LetterBag::getLetterBag() {
        return this->letters;
    }

    void LetterBag::setLetterBag(vector<char> letterSet) {
        this->letters = letterSet;
    }

    void LetterBag::setLetterFreqMap(unordered_map<char, int> letterFreq) {
        this->letterFreqMap = letterFreq;
    }

    void LetterBag::addLetterToFreqMap(char letter, int freq) {
        this->letterFreqMap[letter] = freq;
    }

    LetterBag::~LetterBag()
    {
        //dtor
    }
}
