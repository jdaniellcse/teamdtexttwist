#ifndef WORDBANK_H
#define WORDBANK_H

#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

namespace model {
class WordBank
    {
        public:
            WordBank();
            virtual ~WordBank();

            void addToWordBank(const string& word, int pointValue);
            unordered_map<string, int> getAvailableWords();
            void setWordCount();
            int getWordCount();
            void removeUsedWord(const string& word);
        protected:
        private:
            unordered_map<string, int> availableWords;
            int wordCount;
    };
}

#endif // WORDBANK_H
