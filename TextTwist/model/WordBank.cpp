#include "WordBank.h"

namespace model {
    WordBank::WordBank()
    {
        this->availableWords = unordered_map<string, int>();
    }

    void WordBank::addToWordBank(const string& word, int pointValue) {
        this->availableWords[word] = pointValue;
    }

    unordered_map<string, int> WordBank::getAvailableWords() {
        return this->availableWords;
    }

    void WordBank::setWordCount() {
        this->wordCount = this->availableWords.size();
    }

    int WordBank::getWordCount() {
        return this->wordCount;
    }

    void WordBank::removeUsedWord(const string& word) {
        this->availableWords.erase(word);
    }

    WordBank::~WordBank()
    {
        //dtor
    }
}
