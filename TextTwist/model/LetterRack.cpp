#include "LetterRack.h"

namespace model {

    LetterRack::LetterRack()
    {
        this->letterRack = vector<char>();
        this->letterCount = 7;
    }

    void LetterRack::initializeLetterRack(vector<char> letters) {
        for(int i = 0; i < this->letterCount; i++) {
            this->letterRack.push_back(letters[i]);
        }
    }

    void LetterRack::twistLetterRack() {
        std::srand (unsigned (std::time(0)));
        std::random_shuffle (this->letterRack.begin(), this->letterRack.end());
    }

    vector<char> LetterRack::getLetterRack() {
        return this->letterRack;
    }

    LetterRack::~LetterRack()
    {
        //dtor
    }
}
