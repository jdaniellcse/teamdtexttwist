#ifndef LETTERRACK_H
#define LETTERRACK_H

#include <vector>
#include <ctime>
#include <algorithm>
#include <cstdlib>
using namespace std;

#include "LetterBag.h"
namespace model {

    class LetterRack
    {
        public:
            LetterRack();
            virtual ~LetterRack();

            void initializeLetterRack(vector<char> letters);
            vector<char> getLetterRack();
            void twistLetterRack();
        protected:
        private:
            int letterCount;
            vector<char> letterRack;
    };
}

#endif // LETTERRACK_H
